package andreas.develops.proyek.kelompok;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

public class DatabaseHandler extends SQLiteOpenHelper {
    public static final String TABLE_NAME = "data_motor";
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_MEREK = "merek_motor";
    public static final String COLUMN_HARGA = "harga_motor";
    public static final String COLUMN_GAMBAR = "gambar_motor";
    public static final String COLUMN_STATUS = "status";
    private static final String DATABASE_NAME ="booking";
    private static final int DATABASE_VERSION=1;
//    private static int gambarnya = R.drawable.motor;

    private static final String db_motor = "create table "
            + TABLE_NAME + "("
            + COLUMN_ID +" integer primary key autoincrement, "
            + COLUMN_MEREK+ " varchar(50) not null, "
            + COLUMN_HARGA+ " varchar(50) not null, "
            + COLUMN_STATUS+ " varchar(50) not null, "
            + COLUMN_GAMBAR+ " int not null);";

    private static final String db_user = "create table "
            + "user" + "("
            + COLUMN_ID +" integer primary key autoincrement, "
            + "nama_user"+ " varchar(50) not null, "
            + "role"+ " int not null, "
            + "username_user"+ " varchar(50) not null, "
            + "password_user"+ " varchar(50) not null);";

    private static final String db_pemesanan = "create table "
            + "data_pemesanan" + "("
            + COLUMN_ID +" integer primary key autoincrement, "
            + "id_pemesan"+ " long not null, "
            + "id_produk"+ " integer not null, "
            + "id_petani"+ " long not null,"
            + "produk"+ " varchar(50) not null, "
            + "gambar"+ " BLOB not null, "
            + "total"+ " integer not null, "
            + "tanggal"+ " varchar(50) not null);";


   // private static final String insert = "INSERT INTO data_motor(merek_motor, harga_motor, status, gambar_motor)values('Vario','Rp. 450.000','Tersedia',"+gambarnya+")";
    private static final String insertu = "INSERT INTO user(nama_user,role, username_user, password_user)values('Julio',1,'JuBel','JuBel')";
    private static final String insertu2 = "INSERT INTO user(nama_user,role, username_user, password_user)values('Julio2',2,'JuBel2','JuBel')";
    private static final String insertu3 = "INSERT INTO user(nama_user,role, username_user, password_user)values('Julio3',2,'JuBel3','JuBel')";

    public DatabaseHandler(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(db_motor);
        db.execSQL(db_user);
        db.execSQL(db_pemesanan);
        //db.execSQL(insert);
        db.execSQL(insertu);
        db.execSQL(insertu2);
        db.execSQL(insertu3);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXISTS "+TABLE_NAME);
        onCreate(db);

    }

//    public void addRecordMotor(ModelMotor um){
//        SQLiteDatabase db = getWritableDatabase();
//
//        ContentValues values = new ContentValues();
//        values.put(COLUMN_MEREK, um.getMerek_motor());
//        values.put(COLUMN_HARGA, um.getHarga_motor());
//        values.put(COLUMN_STATUS, um.getStatus());
//        values.put(COLUMN_GAMBAR, um.getGambar());
//
//
//        db.insert(TABLE_NAME,null, values);
//        db.close();
//    }

    public void addUser(ModelUser um){
        SQLiteDatabase db = getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("nama_user", um.getNama_user());
        values.put("role", um.getRole());
        values.put("username_user", um.getUsername());
        values.put("password_user", um.getPassword());


        db.insert("user",null, values);
        db.close();
    }

    public void addPemesanan(ModelPemesanan pe){
        SQLiteDatabase db = getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("id_pemesan",pe.getId_pemesan());
        values.put("id_produk",pe.getId_produk());
        values.put("id_petani",pe.getId_petani());
        values.put("total",pe.getTotal());
        values.put("produk",pe.getProduk());
        values.put("tanggal",pe.getTanggal());
        values.put("gambar1",pe.getGambar1());

        db.insert("data_pemesanan",null, values);
        db.close();

    }

//    public void addRecordBooking(ModelDataBooking um){
//        SQLiteDatabase db = getWritableDatabase();
//
//        ContentValues values = new ContentValues();
//        values.put("id_user", um.getId_user());
//        values.put("id_motor", um.getId_motor());
//        values.put("nama_pembooking", um.getNama_pembooking());
//        values.put("merek_motor", um.getMerek_motor());
//        values.put("harga_motor", um.getHarga_motor());
//        values.put("gambar", um.getGambar());
//        values.put("tanggal_mulai", um.getTanggal_mulai());
//        values.put("status", um.getStatus());
//
//        db.insert("data_booking",null, values);
//        db.close();
//        System.out.println("Berhasil Ditambahkan");
//    }
//
//    public ArrayList<ModelMotor> getAllRecordMotorUser(){
//        ArrayList<ModelMotor> motorList = new ArrayList<ModelMotor>();
//
//        String sql = "SELECT * FROM data_motor WHERE status like 'Tersedia'";
//
//        SQLiteDatabase db = this.getReadableDatabase();
//        Cursor cursor = db.rawQuery(sql,null);
//
//        if(cursor.moveToFirst()){
//            do{
//                ModelMotor userModels = new ModelMotor();
//                userModels.setId(Integer.parseInt(cursor.getString(0)));
//                userModels.setMerek_motor(cursor.getString(1));
//                userModels.setHarga_motor(cursor.getString(2));
//                userModels.setStatus(cursor.getString(3));
//                userModels.setGambar(cursor.getInt(4));
//                motorList.add(userModels);
//            }while(cursor.moveToNext());
//        }
//
//        cursor.close();
//
//        return motorList;
//    }

//    public ArrayList<ModelDataBooking> getAllRecordBooking(long id){
//        ArrayList<ModelDataBooking> motorList = new ArrayList<ModelDataBooking>();
//
//        String sql = "SELECT * FROM data_booking WHERE id_user ="+id;
//
//        SQLiteDatabase db = this.getReadableDatabase();
//        Cursor cursor = db.rawQuery(sql,null);
//
//        if(cursor.moveToFirst()){
//            do{
//                ModelDataBooking bookingModels = new ModelDataBooking();
//                bookingModels.setId(Integer.parseInt(cursor.getString(0)));
//                bookingModels.setId_user(Integer.parseInt(cursor.getString(1)));
//                bookingModels.setId_motor(Integer.parseInt(cursor.getString(2)));
//                bookingModels.setNama_pembooking(cursor.getString(3));
//                bookingModels.setMerek_motor(cursor.getString(4));
//                bookingModels.setHarga_motor(cursor.getString(5));
//                bookingModels.setGambar(Integer.parseInt(cursor.getString(6)));
//                bookingModels.setTanggal_mulai(cursor.getString(7));
//                bookingModels.setStatus(cursor.getString(8));;
//                motorList.add(bookingModels);
//            }while(cursor.moveToNext());
//        }
//
//        cursor.close();
//
//        return motorList;
//    }
//
//    public ArrayList<ModelDataBooking> getAllRecordBooking(String status){
//        ArrayList<ModelDataBooking> motorList = new ArrayList<ModelDataBooking>();
//
//        String sql = "SELECT * FROM data_booking WHERE status like '"+status+"'";
//
//        SQLiteDatabase db = this.getReadableDatabase();
//        Cursor cursor = db.rawQuery(sql,null);
//
//        if(cursor.moveToFirst()){
//            do{
//                ModelDataBooking bookingModels = new ModelDataBooking();
//                bookingModels.setId(Integer.parseInt(cursor.getString(0)));
//                bookingModels.setId_user(Integer.parseInt(cursor.getString(1)));
//                bookingModels.setId_motor(Integer.parseInt(cursor.getString(2)));
//                bookingModels.setNama_pembooking(cursor.getString(3));
//                bookingModels.setMerek_motor(cursor.getString(4));
//                bookingModels.setHarga_motor(cursor.getString(5));
//                bookingModels.setGambar(Integer.parseInt(cursor.getString(6)));
//                bookingModels.setTanggal_mulai(cursor.getString(7));
//                bookingModels.setStatus(cursor.getString(8));;
//                motorList.add(bookingModels);
//            }while(cursor.moveToNext());
//        }
//
//        cursor.close();
//
//        return motorList;
//    }
//
//    public ArrayList<ModelMotor> getAllRecordMotor(){
//        ArrayList<ModelMotor> motorList = new ArrayList<ModelMotor>();
//
//        String sql = "SELECT * FROM data_motor";
//
//        SQLiteDatabase db = this.getReadableDatabase();
//        Cursor cursor = db.rawQuery(sql,null);
//
//        if(cursor.moveToFirst()){
//            do{
//                ModelMotor userModels = new ModelMotor();
//                userModels.setId(Integer.parseInt(cursor.getString(0)));
//                userModels.setMerek_motor(cursor.getString(1));
//                userModels.setHarga_motor(cursor.getString(2));
//                userModels.setStatus(cursor.getString(3));
//                userModels.setGambar(cursor.getInt(4));
//                motorList.add(userModels);
//            }while(cursor.moveToNext());
//        }
//
//        cursor.close();
//
//        return motorList;
//    }

    public ArrayList<ModelUser> getAllRecordUser(){
        ArrayList<ModelUser> as = new ArrayList<ModelUser>();

        String sql = "SELECT * FROM user";

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(sql,null);

        if(cursor.moveToFirst()){
            do{
                ModelUser userModels = new ModelUser();
                userModels.setId(Integer.parseInt(cursor.getString(0)));
                userModels.setNama_user(cursor.getString(1));
                userModels.setRole(cursor.getInt(2));
                userModels.setUsername(cursor.getString(3));
                userModels.setPassword(cursor.getString(4));
                as.add(userModels);
            }while(cursor.moveToNext());
        }

        cursor.close();

        return as;
    }

    public Cursor viewData(){
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "select * from pemesanan";
        Cursor cursor = db.rawQuery(query,null);

        return cursor;
    }

//    public static ModelMotor dataM(Integer id){
//        String sql = "SELECT * FROM data_motor WHERE id = "+id;
//        SQLiteDatabase db = DatabaseHandler.getReadableDatabase();
//        Cursor cursor = db.rawQuery(sql, null);
//
//        ModelMotor userModels = new ModelMotor();
//        userModels.setId(Integer.parseInt(cursor.getString(0)));
//        userModels.setMerek_motor(cursor.getString(1));
//        userModels.setHarga_motor(cursor.getString(2));
//        userModels.setStatus(cursor.getString(3));
//        userModels.setGambar(cursor.getInt(4));
//
//        return userModels;
//    }

//    public static String getMotorModelCount(long id, Context a){
//        DatabaseHandler as = new DatabaseHandler(a);
//        String sql = "SELECT * FROM data_booking where id = id";
//        SQLiteDatabase db = as.getWritableDatabase();
//        Cursor cursor = db.rawQuery(sql, null);
//        String abc = cursor.getString(6);
//        cursor.close();
//        return abc;
//    }

//    public static void batalkan(long id, long id_motor, Context a){
//        DatabaseHandler as = new DatabaseHandler(a);
//        String sql = "UPDATE data_booking SET status = 'Dibatalkan' where id = "+id;
//        SQLiteDatabase db = as.getWritableDatabase();
//        Cursor cursor = db.rawQuery(sql, null);
//        cursor.close();
//        buat_sedia(id_motor, a);
//
//    }
//
//    public static void kembalikan(long id, long id_motor, Context a){
//        DatabaseHandler as = new DatabaseHandler(a);
//        String sql = "UPDATE data_booking SET status = 'Selesai' where id = "+id;
//        SQLiteDatabase db = as.getWritableDatabase();
//        Cursor cursor = db.rawQuery(sql, null);
//        cursor.close();
//        buat_sedia(id_motor, a);
//    }

//    public static void buat_sedia(long id, Context a){
//        DatabaseHandler as = new DatabaseHandler(a);
//        String sql = "UPDATE data_motor SET status = 'Tersedia' where id = "+id;
//        SQLiteDatabase db = as.getWritableDatabase();
//        Cursor cursor = db.rawQuery(sql, null);
//        cursor.close();
//    }


}
