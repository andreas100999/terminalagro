package andreas.develops.proyek.kelompok;

public class ModelUser {

    private long id;
    private String nama_user;
    private String username;
    private String password;
    private int role;

    public ModelUser() {
    }

    public ModelUser(long id, String nama_user, String username, String password, int role) {
        this.id = id;
        this.nama_user = nama_user;
        this.username = username;
        this.password = password;
        this.role = role;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNama_user() {
        return nama_user;
    }

    public void setNama_user(String nama_user) {
        this.nama_user = nama_user;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getRole() {
        return role;
    }

    public void setRole(int role) {
        this.role = role;
    }
}
