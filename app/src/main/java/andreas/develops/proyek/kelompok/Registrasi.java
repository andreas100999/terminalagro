package andreas.develops.proyek.kelompok;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;



public class Registrasi extends AppCompatActivity {

    EditText nama, username, password;
    DatabaseHandler dbh;
    Registrasi a;
    public  static int userRole;

    Spinner spinnerDropDownView;
    String[] spinnerValueHoldValue = {"Customer", "Petani"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrasi);

        nama = (EditText)findViewById(R.id.nama);
        username = (EditText) findViewById(R.id.username);
        password = (EditText) findViewById(R.id.password);
        a = this;
        dbh = new DatabaseHandler(a);

        spinnerDropDownView =(Spinner)findViewById(R.id.spinner1);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(Registrasi.this, android.R.layout.simple_list_item_1, spinnerValueHoldValue);
        spinnerDropDownView.setAdapter(adapter);

        spinnerDropDownView.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(Registrasi.this, spinnerDropDownView.getSelectedItem().toString(), Toast.LENGTH_LONG).show();
                if(spinnerDropDownView.getSelectedItem().equals("Customer")){
                    userRole = 2;
                }else {
                    userRole = 1;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void daftar(View view) {
        ModelUser mu = new ModelUser();
        mu.setNama_user(nama.getText().toString());
        mu.setUsername(username.getText().toString());
        mu.setPassword(password.getText().toString());
        mu.setRole(userRole);

        dbh.addUser(mu);

        Toast.makeText(getApplicationContext(), "Anda berhasil ditambahkan dan akan kembali dalam 2 detik", Toast.LENGTH_SHORT).show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                final Intent mainIntent = new Intent(getApplicationContext(),
                        Login.class);
                startActivity(mainIntent);
                finish();
            }
        }, 2000);//delay 5 detik

    }
}