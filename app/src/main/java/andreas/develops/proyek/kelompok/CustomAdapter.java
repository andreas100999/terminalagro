package andreas.develops.proyek.kelompok;

import android.content.ClipData;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.content.ClipData.Item;

import java.util.ArrayList;

public class CustomAdapter extends BaseAdapter {
    private Context context;
    private  int layout;
    private ArrayList<ModelPemesanan> foodsList;
    LayoutInflater inflter;

    public CustomAdapter(Context context, int layout, ArrayList<ModelPemesanan> foodsList) {
        this.context = context;
        this.layout = layout;
        this.foodsList = foodsList;
    }

    @Override
    public int getCount() {
        return foodsList.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        ModelPemesanan modelPemesanan = (ModelPemesanan) getItem(i);

        if(view == null){
            view = LayoutInflater.from(context).inflate(R.layout.activity_listview1,viewGroup,false);
        }

        ModelPemesanan currenItem = (ModelPemesanan) getItem(i);

        TextView tx1 = (TextView)view.findViewById(R.id.textView);

        tx1.setText("beras");

        return view;
//
//        view = inflter.inflate(R.layout.activity_listview1, null);
//        TextView country = (TextView) view.findViewById(R.id.textView);
//        TextView beli = (TextView) view.findViewById(R.id.beli);
//        ImageView icon = (ImageView) view.findViewById(R.id.icon);
//
//        ModelPemesanan md = foodsList.get(i);
//
//        country.setText(md.getProduk());
//        return view;
    }
}
