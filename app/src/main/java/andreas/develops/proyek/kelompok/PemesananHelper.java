package andreas.develops.proyek.kelompok;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;

import static andreas.develops.proyek.kelompok.Login.idLogin;

public class PemesananHelper extends SQLiteOpenHelper {
    public PemesananHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    public void queryData(String sql){
        SQLiteDatabase database = getWritableDatabase();
        database.execSQL(sql);
    }
    public void pemesananData(long id_pemesan,long id_petani,int id_produk,String produk,int total,String tanggal,byte[] gambar){
        SQLiteDatabase database = getWritableDatabase();
        String sql = "INSERT INTO PEMESANAN VALUES (NULL, ?, ?, ?, ?, ?, ?, ?)";

        SQLiteStatement statement = database.compileStatement(sql);
        statement.clearBindings();

        statement.bindLong(1, id_pemesan);
        statement.bindLong(2, id_petani);
        statement.bindDouble(3, id_produk);
        statement.bindString(4,produk);
        statement.bindLong(5,total);
        statement.bindString(6,tanggal);
        statement.bindBlob(7,gambar);


        statement.executeInsert();
    }



    @Override
    public void onCreate(SQLiteDatabase db) {

    }
    public Cursor getData(String sql){
        SQLiteDatabase database = getReadableDatabase();
        return database.rawQuery(sql, null);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
