package andreas.develops.proyek.kelompok;

public class ModelPemesanan {
    public int id,id_produk,total;
    public long id_pemesan,id_petani;
    public String produk,tanggal;
    public byte[] gambar1;

    public ModelPemesanan(){

    }

    public ModelPemesanan(int id, int id_produk, int total, long id_pemesan, long id_petani, String produk, String tanggal, byte[] gambar1) {
        this.id = id;
        this.id_produk = id_produk;
        this.total = total;
        this.id_pemesan = id_pemesan;
        this.id_petani = id_petani;
        this.produk = produk;
        this.tanggal = tanggal;
        this.gambar1 = gambar1;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_produk() {
        return id_produk;
    }

    public void setId_produk(int id_produk) {
        this.id_produk = id_produk;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public long getId_pemesan() {
        return id_pemesan;
    }

    public void setId_pemesan(long id_pemesan) {
        this.id_pemesan = id_pemesan;
    }

    public long getId_petani() {
        return id_petani;
    }

    public void setId_petani(long id_petani) {
        this.id_petani = id_petani;
    }

    public String getProduk() {
        return produk;
    }

    public void setProduk(String produk) {
        this.produk = produk;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public byte[] getGambar1() {
        return gambar1;
    }

    public void setGambar1(byte[] gambar1) {
        this.gambar1 = gambar1;
    }
}
