package andreas.develops.proyek.kelompok;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class firebase extends AppCompatActivity {

    EditText editTextSearch;
    Button btnSearch;
    TextView txtResults;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_firebase);

        editTextSearch = (EditText)findViewById(R.id.editTxtSearch);
        btnSearch = (Button)findViewById(R.id.btnSubmit);
        txtResults = (TextView)findViewById(R.id.txtViewResult);
        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(TextUtils.isEmpty(editTextSearch.getText().toString())){
                    Toast.makeText(firebase.this,"No empty Keyword",Toast.LENGTH_SHORT).show();
                }else{
                    DatabaseReference mRef = FirebaseDatabase.getInstance().getReference("meaning");
                    mRef.addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            String searKeyword = editTextSearch.getText().toString();
                            if(dataSnapshot.child(searKeyword).exists()){
                                txtResults.setText(dataSnapshot.child(searKeyword).getValue().toString());
                            }else{
                                Toast.makeText(firebase.this,"No search Result",Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });
                }
            }
        });
    }
}
