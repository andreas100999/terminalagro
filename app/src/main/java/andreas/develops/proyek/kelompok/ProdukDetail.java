package andreas.develops.proyek.kelompok;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import static andreas.develops.proyek.kelompok.Login.idLogin;


public class ProdukDetail extends AppCompatActivity {

    TextView nama,harga;
    ImageView gambar1;
    DatabaseHandler dbh;
    EditText jumlah1;
    Button but;

    public static PemesananHelper pemesananHelper;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_produk_detail);

        but = (Button)findViewById(R.id.but);
        nama = (TextView)findViewById(R.id.nama);
        harga = (TextView)findViewById(R.id.harga);
        gambar1 = (ImageView)findViewById(R.id.gambar);
        jumlah1 = (EditText)findViewById(R.id.jumlah);

        Intent intent = getIntent();
        int id = intent.getExtras().getInt("id");
        String nama1 = intent.getExtras().getString("nama");
        String harga1 = intent.getExtras().getString("harga");
        byte[] foodImage = intent.getExtras().getByteArray("gambar1");
        final Bitmap bitmap = BitmapFactory.decodeByteArray(foodImage, 0, foodImage.length);


        //int image1 = intent.getExtras().getInt("gambar1");

        nama.setText(nama1);
        harga.setText(harga1);
        gambar1.setImageBitmap(bitmap);

        pemesananHelper = new PemesananHelper(this,"Pemesanan1.sqlite",null,1);

        pemesananHelper.queryData("CREATE TABLE IF NOT EXISTS PEMESANAN(Id INTEGER PRIMARY KEY AUTOINCREMENT, id_pemesan long, id_petani long, id_produk int,produk String , total int,tanggal String , image BLOB)");

        but.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{
                    Intent intent = getIntent();
                    pemesananHelper.pemesananData(idLogin,3,intent.getExtras().getInt("id"),intent.getExtras().getString("nama"),Integer.parseInt(jumlah1.getText().toString()),"12 asd",intent.getExtras().getByteArray("gambar1"));
                    Toast.makeText(getApplicationContext(),"berhasil",Toast.LENGTH_LONG).show();
                }catch(Exception e){

                }
            }
        });
    }

    public void tambahPemesanan(View view){
        Intent intent = getIntent();
        String nama1 = intent.getExtras().getString("nama");
        String harga1 = intent.getExtras().getString("harga");
        byte[] foodImage = intent.getExtras().getByteArray("gambar1");
        Bitmap bitmap = BitmapFactory.decodeByteArray(foodImage, 0, foodImage.length);

        ModelPemesanan mu = new ModelPemesanan();
        mu.setId_pemesan(idLogin);
        mu.setId_petani(2);
        mu.setId_produk(3);
        mu.setProduk("asd");
        mu.setTotal(312);
        mu.setTanggal("asd");
        mu.setGambar1(intent.getExtras().getByteArray("gambar1"));

        dbh.addPemesanan(mu);

        Toast.makeText(getApplicationContext(),"Pemesanan berhasil",Toast.LENGTH_LONG).show();
    }


}
