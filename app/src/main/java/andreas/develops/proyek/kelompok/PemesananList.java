package andreas.develops.proyek.kelompok;

import android.app.Activity;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

import static andreas.develops.proyek.kelompok.Login.idLogin;

public class PemesananList extends Activity {
    ListView simpleList;
    String countryList[] = {"Audio"};
    int flags[] = {R.drawable.logo};

    ArrayList<ModelPemesanan> list;
    ProdukListAdapter adapter = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pemesanan_list);

        list = new ArrayList<>();


        simpleList = (ListView) findViewById(R.id.simpleListView);
        CustomAdapter customAdapter = new CustomAdapter(this,R.layout.activity_listview1, list);
        simpleList.setAdapter(customAdapter);

        Cursor cursor = ProdukDetail.pemesananHelper.getData("SELECT * FROM PEMESANAN");
        list.clear();
        while (cursor.moveToNext()) {
            int id = cursor.getInt(0);
            long id_pemesan = cursor.getLong(1);
            long id_petani = cursor.getLong(2);
            int id_produk = cursor.getInt(3);
            String produk = cursor.getString(4);
            int total = cursor.getInt(5);
            String tanggal = cursor.getString(6);
            byte[] image = cursor.getBlob(7);
            list.add(new ModelPemesanan(1,2,3,3,2,"beras","12 september",image));
        }

    }
}
