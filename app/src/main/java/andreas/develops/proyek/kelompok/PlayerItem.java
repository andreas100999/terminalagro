package andreas.develops.proyek.kelompok;
import java.io.Serializable;

public class PlayerItem implements Serializable {
    String no, name, Position, birth_date, Poster;

    public PlayerItem(String no, String name, String Position, String Poster ) {
        this.no = no;
        this.name = name;
        this.Position = Position;
        this.Poster = Poster;
    }


    public String getNo() {
        return no;
    }

    public String getName() {
        return name;
    }

    public String getPosition() {
        return Position;
    }

    public String getPoster() {
        return Poster;
    }
}